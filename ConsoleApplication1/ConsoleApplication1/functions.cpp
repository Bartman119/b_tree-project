
#include "stdafx.h"
#include "vld.h"
// #include"ConsoleApplication1.cpp"
#include <iostream>
#include <list>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string.h>
#include "Functions.h"
#include "Structures.h"
#include "DeletionFunctions.h"

// #define not !
// #define and &&
// #define or ||

void splitChild(node * & pFather,int i, node * & pRoot) 
{
	node *greaterNode = new node{ pRoot->order, pRoot->isLeaf };	// Create a new node which is going to store d-1 keys of pRoot
	greaterNode->keyAmount = pRoot->order - 1;
	auto pMiddle = pRoot->keyList; // Copy the last d-1 keys of pRoot to new node
	for (int j = 0; j < pRoot->order; j++) // Move to the first value after median
		pMiddle = pMiddle->pNext;
	auto pGreaterValues = greaterNode->keyList;
	nodeValue *nV2 = new nodeValue{ pMiddle->key,nullptr,nullptr }; //create first space for value and move higher values to greaterNode
	while (pMiddle->pNext)
	{
		nV2 = new nodeValue{ pMiddle->pNext->key,nV2,nullptr };
		nV2->pPrev->pNext = nV2;
		pMiddle = pMiddle->pNext;
	}
	//delete pMiddle;
	while (nV2->pPrev)
		nV2 = nV2->pPrev;
	pGreaterValues = nV2; //cut higher values from pRoot
	greaterNode->keyList = pGreaterValues;
	auto pGreaterSons = greaterNode->sonsList;
	if (pRoot->isLeaf == false) // Copy the last d children of pRoot to new node 
	{
		auto pMiddle2 = pRoot->sonsList;
		for (int j = 0; j < pRoot->order; j++) // Move to the middle son
			pMiddle2 = pMiddle2->pNext;
		pGreaterSons = pMiddle2; // Assign last sons of pRoot to new node
		pGreaterSons->pPrev = nullptr;
		greaterNode->sonsList = pGreaterSons;
		auto pMiddle4 = pRoot->sonsList;
		for (int j = 0; j < pRoot->order - 1 ; j++) 
			pMiddle4 = pMiddle4->pNext;
		pMiddle4->pNext = nullptr;
		while (pMiddle4->pPrev)
			pMiddle4 = pMiddle4->pPrev;
		pRoot->sonsList = pMiddle4;
	}
	else //if pRoot doesnt have any sons, both nodes (children) become leaves
	{
		pRoot->isLeaf = true;
		greaterNode->isLeaf = true;
	}
	pRoot->keyAmount = pRoot->order - 1; // Reduce the number of keys in pRoot 
	auto pLastSon = pFather->sonsList; // Create a pointer to new child of pFather
	while (pLastSon->pNext)
		pLastSon = pLastSon->pNext;
	nodeSon *nS = new nodeSon{ pLastSon,nullptr,greaterNode };
	if (pLastSon->pPrev == nullptr)
		pLastSon->pNext = nS;
	else 
	{
		pLastSon->pNext = nS;
		while (pLastSon->pChild != pRoot)
		{
			auto pTemp = pLastSon->pChild;
			pLastSon->pChild = pLastSon->pNext->pChild;
			pLastSon->pNext->pChild = pTemp;
			pLastSon = pLastSon->pPrev;
		}
	}
	auto pLastKey = pFather->keyList; // Find a place to copy median to pFather
	auto pMiddle3 = pRoot->keyList;
	for (int j = 0; j < pRoot->order - 1; j++) // Move to the middle value
		pMiddle3 = pMiddle3->pNext;
	if (pLastKey == nullptr) //if there's no values create a new one
	{
		pLastKey = new nodeValue{ pMiddle3->key,nullptr,nullptr, };
		pFather->keyList = pLastKey;
	}
	else 
	{
		while (pLastKey->pNext) // Move to last key in father
			pLastKey = pLastKey->pNext;
		nodeValue *nV = new nodeValue{ 0, pLastKey,nullptr };
		pLastKey->pNext = nV;
		pLastKey = pLastKey->pNext;
		bool isBigger = true; //condition, where new key should be added at the beggining used below
		auto pValue = pFather->keyList;
		while ((pLastKey->pPrev) and (pLastKey->pPrev->key > pMiddle3->key)/*(i + 1 >= 0)*/) //infinite loop!
		{
			isBigger = false;
			auto pTemp = pLastKey->pPrev->key;
			pLastKey->pPrev->key = pLastKey->key;
			pLastKey->key = pTemp;
			pLastKey = pLastKey->pPrev;
			i--;
		}
		if (isBigger == true)
			pLastKey->key = pMiddle3->key;
		else
			pLastKey->key = pMiddle3->key;
	}
	pFather->keyAmount++; // Increment count of keys in this node 
	auto pDelete = pRoot->keyList; //deleting values from pRoot
	
	/*while (pDelete->pPrev)
		pDelete = pDelete->pPrev;
*/
	while (pDelete->pNext)
		pDelete = pDelete->pNext;
	for (int k = 0; k < pRoot->order; k++) //pDelete loses pointers to previous values bc of previous operations on them, i may have to rework code above
	{
		pDelete = pDelete->pPrev;
		//delete pDelete->pNext;
		pDelete->pNext->pPrev = nullptr;
		delete pDelete->pNext;
		pDelete->pNext = nullptr;

	}
}

void insertNonFull(node * & pRoot, const type & value)
{
	int i = pRoot->keyAmount;
	auto pRightmost = pRoot->keyList; // Initialize index as index of rightmost element 
	while (pRightmost->pNext) // Move to the rightmost value 
		pRightmost= pRightmost->pNext;
	// If this is a leaf node 
	if (pRoot->isLeaf == true)
	{
		nodeValue *nV = new nodeValue;
		nV->pNext = nullptr;
		nV->pPrev = pRightmost;
		nV->key = 0;
		pRightmost->pNext = nV;
		int j = 0;
		while (pRightmost && value < pRightmost->key) // The following loop does two things
		{											  // a) Finds the location of new key to be inserted 
			j++;									  // b) Moves all greater keys to one place ahead
			if (!pRightmost->pPrev) //if it's the first element and it needs moving
			{
				pRightmost->pNext->key = pRightmost->key;
				pRightmost->key = value;
			}
			else
			{
				auto pTemp = pRightmost->key;
				pRightmost->key = pRightmost->pNext->key;
				pRightmost->pNext->key = pTemp;
				if (value < pRightmost->pPrev->key) //if the previous value in list is not bigger then do not move pointer
					pRightmost = pRightmost->pPrev;
			}
		}
		if (j == 0) 
			pRightmost->pNext->key = value;
		else								// Insert the new key at found location 
			pRightmost->key = value;
		pRoot->keyAmount++;
	}
	else // If this node is not leaf 
	{
		while ((i >= 0) && (pRightmost->key > value)) // Move through values and find a correct son to send the value to   
		{
			if (pRightmost->pPrev == nullptr) // node has one value and new value goes to first son 
			{
				i--;
				break;
			}
			pRightmost = pRightmost->pPrev;
			i--;
		}
		auto pNewPapa = pRoot;
		auto pNewKeys = pRoot->keyList;
		auto pGoodChild = pRoot->sonsList;
		for (int j = 0; j < i ; j++) // Move to correct son pointer
			pGoodChild = pGoodChild->pNext;
		if (pGoodChild->pChild->keyAmount == 2 * pRoot->order - 1) // See if the found child is full 
		{
			splitChild(pNewPapa, i + 1, pGoodChild->pChild); // If the child is full, then split it
			for (int k = 0; k < i; k++)
				pNewKeys = pNewKeys->pNext;
			if (pNewKeys->key < value) //check which son gets the key
				pGoodChild = pGoodChild->pNext; // Move to other son, probably incorrect
		}
		insertNonFull(pGoodChild->pChild, value);

	}
}

void add(node * & pRoot, const type & value, int d) 
{
	if (!pRoot) //List is empty - create a new node
	{
		nodeValue *nV = new nodeValue{ value,nullptr,nullptr };
		nodeSon *nS = new nodeSon{ nullptr,nullptr,nullptr };
		node *rootNode = new node{d,1,true,nV,nS};
		pRoot = rootNode;	//Set it as a new root

	} 
	else // If tree is not empty 
	{
		// If root is full, then tree grows in height 
		if (pRoot->keyAmount == 2 * d - 1)
		{
			// Create a new node 
			node *newRoot = new node{d,0,false};
			// Make old root as child of new root 
			newRoot->sonsList = new nodeSon{ nullptr,nullptr,pRoot };
			
			// Split the old root and move 1 key to the new root 
			splitChild(newRoot, 0, pRoot);

			// New root has two children now.  Decide which of the 
			// two new children is going to have new key ( left or right) 
			int i = 0;
			auto pWhichSon = newRoot->sonsList;
			if (newRoot->keyList->key < value)
				pWhichSon = pWhichSon->pNext;
			insertNonFull(pWhichSon->pChild, value);
			// Change root 
			pRoot = newRoot;
		}
		else  // If root is not full, call insertNonFull for root 
			insertNonFull(pRoot, value);
	}
}
void graph(node * pRoot, int identation)
{
	cout << endl;
	auto pSynowie = pRoot->sonsList;
	auto pKeys = pRoot->keyList;
	for (int i = 0; i < pRoot->keyAmount; i++)
	{
		if (pRoot->isLeaf == false)
		{
			graph(pSynowie->pChild, identation + 1);	// Move to first son
			pSynowie = pSynowie->pNext;	// And then move to the next one in loop
		}
		cout << setw(identation * 10);
		if (pKeys->pPrev == nullptr)
		{
			cout << "[";
		}
		if (pKeys->pNext == nullptr)
		{
			cout << pKeys->key;
			cout << "]" << endl;
		}
		else
			cout << pKeys->key << endl;
		pKeys = pKeys->pNext;
	}
	// Print the subtree rooted with last child 
	if (pRoot->isLeaf == false)
		graph(pSynowie->pChild, identation + 1);
}
void graph(ostream &s, node * pRoot, int identation)
{
	s << endl;
    auto pSynowie = pRoot->sonsList;
	auto pKeys = pRoot->keyList;
	for (int i = 0; i < pRoot->keyAmount; i++)
	{ 
		if (pRoot->isLeaf == false)
		{
			graph(s,pSynowie->pChild, identation + 1);	// Move to first son
			pSynowie = pSynowie->pNext;	// And then move to the next one in loop
		}
		s << setw(identation*10);
		if (pKeys->pPrev == nullptr) 
		{
			s << "[";
		}
		if (pKeys->pNext == nullptr) 
		{
			s << pKeys->key;
			s << "]" << endl;
		}
		else
			s << pKeys->key << endl;
		pKeys = pKeys->pNext;
	}
	// Print the subtree rooted with last child 
	if (pRoot->isLeaf == false)
		graph(s,pSynowie->pChild,identation+1);
}
void print(node * pRoot)
{
	auto pTop = pRoot->sonsList;
	auto pKey = pRoot->keyList;
	for (int i = 0; i < pRoot->keyAmount; i++)
	{
		// If this is not leaf, then before printing key[i], 
		// traverse the subtree rooted with child C[i]. 
		if (pRoot->isLeaf == false)
		{
			print(pTop->pChild);	// Move to first son
			pTop = pTop->pNext;	// And then move to the next one in loop
		}
		cout << " " << pKey->key;
		pKey = pKey->pNext;
	}
	// Print the subtree rooted with last child 
	if (pRoot->isLeaf == false)
		print(pTop->pChild);
}

void print(ostream &s,node * pRoot)
{
	auto pTop = pRoot->sonsList;
	auto pKey = pRoot->keyList;
	for (int i = 0; i < pRoot->keyAmount; i++)
	{
		// If this is not leaf, then before printing key, 
		// traverse the subtree rooted with child C. 
		if (pRoot->isLeaf == false)
		{
			print(s,pTop->pChild);	// Move to first son
			pTop = pTop->pNext;	// And then move to the next one in loop
		}
		s << " " << pKey->key;
		pKey = pKey->pNext;
	}
	// Print the subtree rooted with last child 
	if (pRoot->isLeaf == false)
		print(s,pTop->pChild);
}

void deleteTree(node * pRoot) 
{
	auto pTop = pRoot->sonsList;
	auto pKey = pRoot->keyList;
	for (int i = 0; i < pRoot->keyAmount; i++)
	{
		// If this is not leaf, then before printing key, 
		// traverse the subtree rooted with child. 
		if (pRoot->isLeaf == false)
		{
			deleteTree(pTop->pChild);	// Move to first son
			pTop = pTop->pNext;	// And then move to the next one in loop
		}	
	}
	while (pKey->pNext) //move to last key
		pKey = pKey->pNext;
	if (pKey->pPrev)
	{
		while (pKey->pPrev) //delete each key one by one starting from the end
		{
			pKey = pKey->pPrev;
			delete pKey->pNext;
			if (!pKey->pPrev)
			{
				delete pRoot->keyList;
				break;
			}
		}
	}
	else
		delete pRoot->keyList;
	// Print the subtree rooted with last child 
	if (pRoot->isLeaf == false)
	{
		deleteTree(pTop->pChild);
		while (pTop->pPrev) //after all the keys have been visited, delete all sons in the list 
		{
			pTop = pTop->pPrev;
			delete pTop->pNext->pChild;
			delete pTop->pNext;
			if (!pTop->pPrev)
			{
				delete pRoot->sonsList->pChild;
				delete pRoot->sonsList;
				break;
			}

		}
	}
	else
		delete pTop;
	
}

bool check_file(const char *x, const  char *format)
{
	unsigned long a = strlen(x), b = strlen(format);

	if (a <= b)
		return false;

	if (strcmp(&x[a - b], format) == 0)
		return true;

	return false;
}



void openfile(string & name, int &order, node *&root)
{
	ifstream file;
	int nline = 0;
	file.open(name);
	if (!file.is_open())
	{
        // lepiej, zeby funkcja zwrocila kod bledu
		cout << "Cannot open the file. Check your input file.";
	}
	else
	{
		stringstream str;
		string check, rest, comm;
		double value;
		bool order_tree = false;
		while (getline(file, check, '\n'))
		{
            // check = usun_komentarz(check);
            
			++nline;
			str = stringstream{ check.data() };
			while (str)
			{
				str >> comm;

				if (comm == "%")
				{
					while (str)
					{
						string tmp;
						str >> tmp;
					}


				}
				if (atoi(comm.c_str()) && order_tree == false)
				{
					size_t found = comm.find('%');
					if (found > 0 && found != string::npos)
					{
						comm.resize(found);
						while (str)
						{
							string tmp;
							str >> tmp;
						}
					}
					try 
					{
				    	order = stoi(comm);
                    }
                    catch (...)
                    {
                        cout << "nieprawidlowy znak"; 
                        return;
                    }
                    if (order < 2)
					{
						cout << "Order cannot be less than 2";
						return;
					}
					order_tree = true;
					str >> comm;
				}
				if (comm == "add")
				{

					while (!str.eof())
					{
						str >> comm;
						size_t found = comm.find('%');
						if (found == 0)
						{
							while (str)
							{
								string tmp;
								str >> tmp;
							}
						}
						else
						{
							if (found > 0 && found != string::npos)
							{
								comm.resize(found);
								while (str)
								{
									string tmp;
									str >> tmp;
								}
							}
							if (stof(comm.c_str()))
							{
								value = stof(comm.c_str());
								add(root, value, order);

							}

							else
							{
								cout << "Invalid input at line: " << nline << endl;
								return;
							}

						}
					}
				}
				if (comm == "remove")  //  do stalej np. REMOVE
				{

					while (!str.eof())
					{
						str >> comm;
						size_t found = comm.find('%');
						if (found == 0)
						{
							while (str)
							{
								string tmp;
								str >> tmp;
							}
						}
						else
						{
							if (found > 0 && found != string::npos)
							{
								comm.resize(found);
								while (str)
								{
									string tmp;
									str >> tmp;
								}
							}
							if (stof(comm.c_str()))
							{
								value = stof(comm.c_str());
								removeValue(root,value);

							}
							else
							{
								cout << "Invalid input at line: " << nline << endl;
								return;
							}
						}
					}

				}
				if (comm == "graph")
				{
					bool append = false;
					bool standard = false;
					string outfile;

					str >> comm;

					if (comm == "+")
					{
						str >> comm;
						size_t found = comm.find('%');
						if (found > 0 && found != string::npos)
						{
							comm.resize(found);
							while (str)
							{
								string tmp;
								str >> tmp;
							}
						}
						append = true;
						outfile = comm;
						ofstream out;
						if (!check_file(outfile.c_str(), ".txt")) // po co?
						{
							cout << "Invalid input at line(check if file format is .txt): " << nline << endl;
						}
						out.open(outfile.c_str(), ios::app);

						graph(out, root, 0);
						out << endl;
						while (str)
						{
							string tmp;
							str >> tmp;
						}

						out.close();
					}

					if (comm == "%" || comm.empty())  /// ??????
					{
						standard = true;
						graph(cout, root, 0);
						cout << endl;
						while (str)
						{
							string tmp;
							str >> tmp;
						}
					}
					if (append == false && standard == false)
					{
						size_t found = comm.find('%');
						if (found > 0 && found != string::npos)
						{
							comm.resize(found);
							while (str)
							{
								string tmp;
								str >> tmp;
							}
						}
						outfile = comm;
						ofstream out;
						if (!check_file(outfile.c_str(), ".txt"))
						{
							cout << "Invalid input at line(check if file format is .txt): " << nline << endl;
						}
						out.open(outfile.c_str());
						graph(out, root, 0);
						out << endl;
						out.close();
						while (str)
						{
							string tmp;
							str >> tmp;
						}
					}
				}
				if (comm == "print")
				{
					bool append = false;
					bool standard = false;
					string outfile;

					str >> comm;

					if (comm == "+")
					{
						str >> comm;
						size_t found = comm.find('%');
						if (found > 0 && found != string::npos)
						{
							comm.resize(found);
							while (str)
							{
								string tmp;
								str >> tmp;
							}
						}
						append = true;
						outfile = comm;
						ofstream out;
						if (!check_file(outfile.c_str(), ".txt"))
						{
							cout << "Invalid input at line(check if file format is .txt): " << nline << endl;
						}
						out.open(outfile.c_str(), ios::app);
						print(out, root);
						out << endl;
						while (str)
						{
							string tmp;
							str >> tmp;
						}
						out.close();
					}

					if (comm == "%" || comm == "\0")
					{
						standard = true;
						print(cout, root);
						cout << endl;
						while (str)
						{
							string tmp;
							str >> tmp;
						}
					}
					if (append == false && standard == false)
					{
						size_t found = comm.find('%');
						if (found > 0 && found != string::npos)
						{
							comm.resize(found);
							while (str)
							{
								string tmp;
								str >> tmp;
							}
						}
						outfile = comm;
						ofstream out;
						if (!check_file(outfile.c_str(), ".txt"))
						{
							cout << "Invalid input at line(check if file format is .txt): " << nline << endl;
						}
						out.open(outfile.c_str());
						print(out, root);
						out << endl;
						out.close();
						while (str)
						{
							string tmp;
							str >> tmp;
						}
					}


				}
				else if (comm != "%"&&comm != ""&&order_tree == false)
				{
					cout << "Wrong input, cannot create b-tree without the order(t>=2).\n Please check your input file";
					return;
				}
			}
		}
	}
	file.close();
}
