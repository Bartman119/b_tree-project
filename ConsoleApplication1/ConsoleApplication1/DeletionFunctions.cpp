#include "stdafx.h"
#include "vld.h"
#include <iostream>
#include <list>
#include <iomanip>
#include "DeletionFunctions.h"
#include "Structures.h"

// A function that returns the index of the first key that is greater 
// or equal to k 
int findKey(node * & pRoot, double k)
{
	auto pValue = pRoot->keyList;
	int idx = 0;
	while (idx < pRoot->keyAmount && pValue->key < k) 
	{
		pValue = pValue->pNext;
		++idx;
	}
	return idx;
}

// A function to remove the idx-th key from this node - which is a leaf node 
void removeFromLeaf(node * & pRoot, int idx)
{
	auto pValue = pRoot->keyList;
	//move to key one after the deleted value
	for (int i = 0; i < idx + 1; i++) 
	{
		if(pValue->pNext)
		pValue = pValue->pNext;
	}
		
	// Move all the keys after the idx-th pos one place backward 
	for (int i = idx + 1; i < pRoot->keyAmount; ++i)
	{
		pValue->pPrev->key = pValue->key;
		if(pValue->pNext)
			pValue = pValue->pNext;
	}
	pValue = pValue->pPrev;
	delete pValue->pNext;
	pValue->pNext = nullptr; //delete last key in keyList
	while (pValue->pPrev) // move back to first key
		pValue = pValue->pPrev;
	pRoot->keyList = pValue; //assign new keylist for a node
	// Reduce the count of keys 
	pRoot->keyAmount--;

	return;
}

// A function to get predecessor of keys[idx] 
int getPred(node * & pRoot,int idx)
{
	auto pCurrentRoot = pRoot;
	auto pCorrectSon = pRoot->sonsList;
	for (int i = 0; i < idx; i++) // go to left son of a key 
		pCorrectSon = pCorrectSon->pNext;
	while (pCurrentRoot->isLeaf == false) 
	{
		pCurrentRoot = pCorrectSon->pChild; //will pcorrectson change? or will i have to assign it again?
		if (pCurrentRoot->sonsList == nullptr)
			continue;
		while (pCurrentRoot->sonsList->pNext)
			pCurrentRoot->sonsList = pCurrentRoot->sonsList->pNext; //move to rightmost node
		pCorrectSon = pCorrectSon->pChild->sonsList;
		while (pCurrentRoot->sonsList->pPrev)
			pCurrentRoot->sonsList = pCurrentRoot->sonsList->pPrev; //move back to correct place
	}
	auto pPredecessor = pCurrentRoot->keyList;
	while (pPredecessor->pNext)
		pPredecessor = pPredecessor->pNext;
	return pPredecessor->key;
}

int getSucc(node * & pRoot, int idx)
{
	auto pCurrentRoot = pRoot;
	auto pCorrectSon = pRoot->sonsList;
	for (int i = 0; i < idx + 1; i++) // go to right son of a key 
		pCorrectSon = pCorrectSon->pNext;
	while (pCurrentRoot->isLeaf == false) //move to leftmost node
	{
		pCurrentRoot = pCorrectSon->pChild; 
		if (pCurrentRoot->sonsList == nullptr)
			continue;
		pCorrectSon = pCorrectSon->pChild->sonsList; //go to leftmost node
	}
	auto pSuccessor = pCurrentRoot->keyList;
	return pSuccessor->key; //return the first key of the leaf
}

void borrowFromPrev(node * & pRoot, int idx)
{
	auto pBorrower = pRoot->sonsList;
	for (int i = 0; i < idx; i++)
		pBorrower = pBorrower->pNext; //move to correct child
	auto pSibling = pRoot->sonsList;
	for (int i = 0; i < idx-1; i++)
		pSibling = pSibling->pNext; //move to sibling that we take the value from
	auto pValue = pRoot->keyList;
	for (int i = 0; i < idx - 1; i++) //move to idx-1 value in pRoot
		pValue = pValue->pNext;
	nodeValue *nV = new nodeValue{pValue->key,nullptr,pBorrower->pChild->keyList }; //add last key from father to child as the first key
	nV->pNext->pPrev = nV;
	pBorrower->pChild->keyList = nV; //setting new keyList for child
	if (pBorrower->pChild->isLeaf == false)  //move last child of sibling as first to child
	{
		while (pSibling->pChild->sonsList->pNext) //move to last sibling's child
			pSibling->pChild->sonsList = pSibling->pChild->sonsList->pNext;
		nodeSon *nS = new nodeSon{ nullptr,pBorrower->pChild->sonsList,pSibling->pChild->sonsList->pChild };
		nS->pNext->pPrev = nS;
		pBorrower->pChild->sonsList = nS;
		pSibling->pChild->sonsList = pSibling->pChild->sonsList->pPrev;
		delete pSibling->pChild->sonsList->pNext;
		pSibling->pChild->sonsList->pNext = nullptr; //STILL HAVE TO DELETE THAT SON FROM SIBLING (FIXED?)
		while (pSibling->pChild->sonsList->pPrev)
			pSibling->pChild->sonsList = pSibling->pChild->sonsList->pPrev;
	} 
	while (pSibling->pChild->keyList->pNext)
		pSibling->pChild->keyList = pSibling->pChild->keyList->pNext;
	pValue->key = pSibling->pChild->keyList->key; //asigning key from sibling to parent
	while (pValue->pPrev) //move the keyList back
		pValue = pValue->pPrev;
	pRoot->keyList = pValue;
	pSibling->pChild->keyList = pSibling->pChild->keyList->pPrev;
	delete pSibling->pChild->keyList->pNext;
	pSibling->pChild->keyList->pNext = nullptr;
	while (pSibling->pChild->keyList->pPrev)
		pSibling->pChild->keyList = pSibling->pChild->keyList->pPrev;
	pBorrower->pChild->keyAmount++;
	pSibling->pChild->keyAmount--;
	return;
}

void borrowFromNext(node * & pRoot, int idx)
{
	auto pBorrower = pRoot->sonsList;
	for (int i = 0; i < idx; i++)
		pBorrower = pBorrower->pNext; //move to correct child
	auto pSibling = pRoot->sonsList;
	for (int i = 0; i < idx + 1; i++)
		pSibling = pSibling->pNext; //move to sibling that we take the value from
	auto pValue = pRoot->keyList;
	for (int i = 0; i < idx; i++) //move to idx value in pRoot
		pValue = pValue->pNext;
	while (pBorrower->pChild->keyList->pNext) //move to last key list child
		pBorrower->pChild->keyList = pBorrower->pChild->keyList->pNext;
	nodeValue *nV = new nodeValue{ pValue->key,pBorrower->pChild->keyList,nullptr }; //assign key from parent to it
	nV->pPrev->pNext = nV;
	pBorrower->pChild->keyList = nV;
	while (pBorrower->pChild->keyList->pPrev) //move the key list back
		pBorrower->pChild->keyList = pBorrower->pChild->keyList->pPrev;
	if(pBorrower->pChild->isLeaf == false) //if child has sons, add first from sibling to it
	{
		auto pBorrowerChild = pBorrower->pChild->sonsList;
		while (pBorrowerChild->pNext) //move to last son in child
			pBorrowerChild = pBorrowerChild->pNext;
		auto pNewSon = pSibling->pChild->sonsList->pChild;	
		nodeSon *nS = new nodeSon{ pBorrowerChild,nullptr,pSibling->pChild->sonsList->pChild }; //add first son of sibling as the last son in child
		//pBorrowerChild->pNext = nS;
		nS->pPrev->pNext = nS;
		pBorrowerChild = nS;
		while (pBorrowerChild->pPrev) //move back to first son
			pBorrowerChild = pBorrowerChild->pPrev;
		pBorrower->pChild->sonsList = pBorrowerChild;
		pSibling->pChild->sonsList->pChild = nullptr;
		delete pSibling->pChild->sonsList->pChild;
		pSibling->pChild->sonsList = pSibling->pChild->sonsList->pNext;
		pSibling->pChild->sonsList->pPrev->pNext = nullptr;
		delete pSibling->pChild->sonsList->pPrev->pNext;	
		delete pSibling->pChild->sonsList->pPrev;
		pSibling->pChild->sonsList->pPrev = nullptr;		
	}
	pValue->key = pSibling->pChild->keyList->key; //assign first key of sibling to parent
	pSibling->pChild->keyList = pSibling->pChild->keyList->pNext; //move to the next item in the keyList
	delete pSibling->pChild->keyList->pPrev;
	pSibling->pChild->keyList->pPrev = nullptr; //delete first pointer from keyList (probably incorrect)
	while (pValue->pPrev)
		pValue = pValue->pPrev;
	pRoot->keyList = pValue;
	pBorrower->pChild->keyAmount++;
	pSibling->pChild->keyAmount--;
	return;
}

// A function to merge C[idx] with C[idx+1] 
// C[idx+1] is freed after merging 
void merge(node * & pRoot, int idx)
{
	auto pMerge = pRoot->sonsList;
	for (int i = 0; i < idx; i++)
		pMerge = pMerge->pNext; //move to correct child
	auto pSibling = pRoot->sonsList;
	for (int i = 0; i < idx + 1; i++) //move to sibling that we take the value from
		pSibling = pSibling->pNext;
	int keys = pSibling->pChild->keyAmount;
	auto pValue = pRoot->keyList;
	for (int i = 0; i < idx; i++) //pull the key and insert it into t-1 position
		pValue = pValue->pNext;
	while (pMerge->pChild->keyList->pNext) //move to last value in child
		pMerge->pChild->keyList = pMerge->pChild->keyList->pNext;
	nodeValue *nV = new nodeValue{ pValue->key,pMerge->pChild->keyList,nullptr };
	nV->pPrev->pNext = nV;
	pMerge->pChild->keyList = nV; //assign new keylist with value from parent, not set back
	while (pValue->pNext) //move all values from the pulled value and delete last one
	{
		pValue->key = pValue->pNext->key;
		pValue = pValue->pNext;
	}
	pValue = pValue->pPrev; //move one line back 
	pValue->pNext = nullptr;
	while (pSibling->pChild->keyList) //move until the end of values
	{
		nodeValue *nV2 = new nodeValue{ pSibling->pChild->keyList->key,pMerge->pChild->keyList,nullptr}; //add each key from sibling to end of child
		nV2->pPrev->pNext = nV2;
		pMerge->pChild->keyList = nV2;
		if (!pSibling->pChild->keyList->pNext)
			break;
		pSibling->pChild->keyList = pSibling->pChild->keyList->pNext;
	}
	while (pMerge->pChild->keyList->pPrev) //move the values back
		pMerge->pChild->keyList = pMerge->pChild->keyList->pPrev;
	while (pSibling->pChild->keyList->pPrev) 
		pSibling->pChild->keyList = pSibling->pChild->keyList->pPrev;
	if (pMerge->pChild->isLeaf == false) 
	{
		while (pMerge->pChild->sonsList->pNext) //move to last son in child
			pMerge->pChild->sonsList = pMerge->pChild->sonsList->pNext;
		while (pSibling->pChild->sonsList) //move until end of sons
		{
			nodeSon *nS = new nodeSon{ pMerge->pChild->sonsList,nullptr,pSibling->pChild->sonsList->pChild};
			pMerge->pChild->sonsList = nS;
			nS->pPrev->pNext = nS;
			if (!pSibling->pChild->sonsList->pNext)
				break;
			pSibling->pChild->sonsList = pSibling->pChild->sonsList->pNext;
		}
		while (pSibling->pChild->sonsList->pPrev) 
			pSibling->pChild->sonsList = pSibling->pChild->sonsList->pPrev;
	}
	while (pSibling->pNext) //moving pointers of parent
	{
		pSibling->pChild = pSibling->pNext->pChild; //replace current child with one in front
		pSibling = pSibling->pNext;
	}
	pSibling = pSibling->pPrev; //deleting the last sibling node
	pSibling->pNext = nullptr;
	while (pSibling->pPrev) //move back to first son
		pSibling = pSibling->pPrev;
	pRoot->sonsList = pSibling; //assign new sonsList to root
	pMerge->pChild->keyAmount += keys + 1; //updating key count
	pRoot->keyAmount--;
	return;
}

// A function to fill child C[idx] which has less than t-1 keys 
void fill(node * & pRoot, int idx)
{
	auto pPrevChild = pRoot->sonsList;
	for (int i = 0; i < idx-1; i++) //move to previous child
		pPrevChild = pPrevChild->pNext;
	auto pNextChild = pRoot->sonsList;
	for (int i = 0; i < idx + 1; i++) //move to next child
		pNextChild = pNextChild->pNext;
	if (idx != 0 && pPrevChild->pChild->keyAmount >= pRoot->order) // If the previous child has more than t-1 keys, borrow a key from that child 
		borrowFromPrev(pRoot, idx);
	else if (idx != pRoot->keyAmount && pNextChild->pChild->keyAmount >= pRoot->order)//hope that 1st condition is correct
		borrowFromNext(pRoot, idx); // If the next child has more than t-1 keys, borrow a key from that child
	// Merge C[idx] with its sibling
	else 
	{
		if (idx != pRoot->keyAmount)// Otherwise merge it with its next sibling 
			merge(pRoot, idx);
		else
			merge(pRoot, idx - 1); // If C[idx] is the last child, merge it with with its previous sibling 
	}
	return;
}


//// A function to remove the idx-th key from this node - which is a non-leaf node 
void removeFromNonLeaf(node * & pRoot, int idx)
{
	auto pKey = pRoot->keyList;
	for (int i = 0; i < idx; i++)
		pKey = pKey->pNext;
	int k = pKey->key;
	auto pPredecessor = pRoot->sonsList;
	for (int j = 0; j < idx; j++) //move to child preceding k (pKey value)
		pPredecessor = pPredecessor->pNext;
	auto pSuccessor = pRoot->sonsList;
	for (int j = 0; j < idx + 1; j++)
		pSuccessor = pSuccessor->pNext;
	// If the child that precedes k has atleast t keys, 
	// find the predecessor 'pred' of k in the subtree rooted at 
	// child. Replace k by pred. Recursively delete pred 
	// in child
	if (pPredecessor->pChild->keyAmount >= pRoot->order) 
	{
		int pred = getPred(pRoot, idx);
		pKey->key = pred;
		while (pKey->pPrev) //move back to start of the keyList
			pKey = pKey->pPrev;
		pRoot->keyList = pKey; //assign changed list in pRoot
		removeValue(pPredecessor->pChild, pred);
	}
	// If the child C[idx] has less that t keys, examine next child. 
	// If next child has atleast t keys, find the successor 'succ' of k in 
	// the subtree rooted at next child 
	// Replace k by succ 
	// Recursively delete succ in next child
	else if (pSuccessor->pChild->keyAmount >= pRoot->order)
	{
		int succ = getSucc(pRoot, idx);
		pKey->key = succ;
		while (pKey->pPrev) //move back to start of the keyList
			pKey = pKey->pPrev;
		pRoot->keyList = pKey; ////assign changed list in pRoot
		removeValue(pSuccessor->pChild,succ);
	}
	// If both C[idx] and C[idx+1] has less that t keys,merge k and all of C[idx+1] 
	// into C[idx] 
	// Now C[idx] contains 2t-1 keys 
	// Free C[idx+1] and recursively delete k from C[idx] 
	else
	{
		merge(pRoot, idx);
		removeValue(pPredecessor->pChild,k); //this is probably incorrect
	}
	return;
}


// A function to remove the key k from the sub-tree rooted with this node 
void removeValue(node * & pRoot,double k)
{
	int idx = findKey(pRoot, k);
	auto pValue = pRoot->keyList;
	for (int i = 0; i < idx; i++) //move to correct value
		pValue = pValue->pNext;
	// The key to be removed is present in this node 
	if (idx < pRoot->keyAmount && pValue->key == k) 
	{
		if (pRoot->isLeaf) // If the node is a leaf node - removeFromLeaf is called 
			removeFromLeaf(pRoot, idx);
		else // Otherwise, removeFromNonLeaf function is called 
			removeFromNonLeaf(pRoot, idx);
	}
	else
	{
		// If this node is a leaf node, then the key is not present in tree 
		if (pRoot->isLeaf) 
		{
			cout << endl;
			cout << "The key " << k << " does not exist in the tree\n";
			return;
		}
		// The key to be removed is present in the sub-tree rooted with this node 
		// The flag indicates whether the key is present in the sub-tree rooted 
		// with the last child of this node 
		bool flag = ((idx == pRoot->keyAmount) ? true : false);
		auto pSon = pRoot->sonsList;
		for (int i = 0; i < idx; i++)
			pSon = pSon->pNext;
		// If the child where the key is supposed to exist has less that t keys, 
		// we fill that child 
		if (pSon->pChild->keyAmount < pRoot->order)
			fill(pRoot,idx);
		// If the last child has been merged, it must have merged with the previous 
		// child and so we recurse on the (idx-1)th child. Else, we recurse on the 
		// (idx)th child which now has atleast t keys
		if (flag && idx > pRoot->keyAmount) 
		{
			auto pPrevSon = pRoot->sonsList;
			for (int i = 0; i < idx - 1; i++) //move to previous son
				pPrevSon = pPrevSon->pNext;
			removeValue(pPrevSon->pChild, k);
		}
		else 
		{
			auto pCurrentSon = pRoot->sonsList;
			for (int i = 0; i < idx; i++) //move to current son, could be unnecessary, we have pson already
				pCurrentSon = pCurrentSon->pNext;
			removeValue(pCurrentSon->pChild, k);
		}		
	}
	return;
}
