#pragma once

#ifndef STRUCTURES_H
#define STRUCTURES_H
// #include <list>

using namespace std;

/**
Type of data stored in a b-tree
*/
typedef int type;

/**
Structure containing all information about values, sons etc in a b-tree
*/
struct node;

/**
Structure holding values for a node in b-tree
@param key held value
@param pPrev pointer to previous value
@param pNext pointer to next value
*/
struct nodeValue 
{
	type key;					
	nodeValue * pPrev, * pNext;	//pointers to next and previous values in node
};

/**
Structure holding sons of a node in b-tree
@param pPrev pointer to previous son
@param pNext pointer to next son
@param pChild pointer to the current child of a node
*/
struct nodeSon //structure holding sons of a node
{
	nodeSon *pPrev, *pNext; //pointers to next and previous sons in node
	node * pChild; //* pFather;  
};

/**
a structure of a node for b-tree
@param order variable containing order of a b-tree
@param isLeaf boolean storing information whether node is a leaf node
@param keylist a container for values
@param sonsList a container for sons
*/
struct node 
{
	int order; //order of a tree
	
	int keyAmount; //the amount of keys currently held in a node
	bool isLeaf; //boolean to check whether node is a leaf
	nodeValue * keyList; // a place to contain values
	nodeSon   * sonsList; // a pointer list to sons of a node
};



#endif 
