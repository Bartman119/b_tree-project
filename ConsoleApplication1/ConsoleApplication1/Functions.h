#pragma once
#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <iostream>
#include "Structures.h"
/**
The function that splits the node into two
@param[in,out] pFather father node of a splitted node
@param i variable used to move through lists
@param[in,out] pChild node that is splitted in a function
@return void
*/
void splitChild(node * & pFather,int i, node * & pChild);
/**
The function that adds a value to a node that is not full
@param[in,out] pRoot pointer to the node in b-tree
@param value value to be inserted to a node
@return void
*/
void insertNonFull(node * & pRoot, const type &  value);

/**
The function that removes pointers after the commands are fulfilled
@param[in,out] pRoot pointer to the first node of a b-tree
@return void
*/
void deleteTree(node * pRoot);

/**
The function checks whether the format of the file is the same as the one that's been assumed
@param x pointer to the format type of the given file
@param format pointer to default assumed format
*/
bool check_file(const char *x, const  char *format);

/**
The function opens a given file with instructions and performs various commands 
@param name name of the file with commands
@param order variable with order of the tree
@param[in,out] root pointer to the root of a b-tree
*/
void openfile(string &name, int &order, node *&root);

/** The function adds a value to a set stored in a b-tree
@param[in, out] pRoot pointer to the first node in a b-tree
@param value value to be added to b-tree
@param d order of b-tree
*/
void add(node * & pRoot, const type & value, int d);


/** The function prints out all numbers in nodes of a b-tree to a standart output
@param[in, out] pRoot pointer to the first node in a b-tree
*/
void print(node * pRoot);

/** The function prints out all numbers in nodes of a b-tree to a file
@param s output stream of a file
@param[in, out] pRoot pointer to the first node in a b-tree
*/
void print(ostream &s, node * pRoot);


/** The function prints a tree of numbers in nodes of a b-tree to a standart output
@param[in, out] pRoot pointer to the first node in a b-tree
*/
void graph(node * pRoot, int identation);

/** The function prints a tree of numbers in nodes of a b-tree to a file
@param s output stream of a file
@param[in, out] pHead pointer to the first node in a b-tree
@param file name of the file where the b-tree will be printed out
*/
void graph(ostream &s,node * pRoot, int identation);

#endif 