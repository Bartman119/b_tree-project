#pragma once
#ifndef DELETIONFUNCTIONS_H
#define DELETIONFUNCTIONS_H

#include <iostream>
#include "Structures.h"

/**
A function to search a key in subtree rooted with this node.
@
*/
//node *search(node * & pRoot, int k);   // returns NULL if k is not present. //dont know if that declaration is correct
/**
A function that returns the index of the first key that is greater or equal to k
@param[in,out] pRoot pointer to the node in which we look for the key
@param k value that we look for
*/
int findKey(node * & pRoot, double k);

/**
A wrapper function to remove the key k in subtree rooted with this node.
@param[in,out] pRoot pointer to the node in which we look for the key to delete
@param k value that we want to delete
*/
void removeValue(node * & pRoot,double k);

/**
A function to remove the key present in idx-th position in this node which is a leaf
@param[in,out] pRoot pointer to the node
@param idx index showing the correct position of deletion 
*/
void removeFromLeaf(node * & pRoot, int idx);

/**
A function to remove the key present in idx-th position in this node which is a non-leaf node
@param[in,out] pRoot pointer to the node
@param idx index showing the correct position of deletion
*/
void removeFromNonLeaf(node * & pRoot, int idx);

/**
A function to get the predecessor of the key where the key is present in the idx-th position in the node
@param[in,out] pRoot pointer to the node
@param idx index showing the correct position of the key
*/
int getPred(node * & pRoot, int idx);

/**
A function to get the successor of the key where the key is present in the idx-th position in the node
@param[in,out] pRoot pointer to the node
@param idx index showing the correct position of the key
*/
int getSucc(node * & pRoot, int idx);

/**
A function to fill up the child node present in the idx-th position in the child list if that child has less than t-1 keys
@param[in,out] pRoot pointer to the node
@param idx index showing the correct position of the key
*/
void fill(node * & pRoot, int idx);

/**
// A function to borrow a key from the previous child node and place it in current child node 
@param[in,out] pRoot pointer to the node
@param idx index showing the correct position of the key
*/
void borrowFromPrev(node * & pRoot, int idx);

/**
// A function to borrow a key from the next child node and place it in current child node
@param[in,out] pRoot pointer to the node
@param idx index showing the correct position of the key
*/
void borrowFromNext(node * & pRoot, int idx);

/**
// A function to merge idx-th child of the node with (idx+1)th child of the node
@param[in,out] pRoot pointer to the node
@param idx index showing the correct position of the key
*/
void merge(node * & pRoot, int idx);




#endif