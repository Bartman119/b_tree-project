// ConsoleApplication1.cpp: definiuje punkt wej?cia dla aplikacji konsolowej.
//

#include "stdafx.h"
//#include <vld.h>
#include "vld.h"
#include <string>
#include <vector>
#include <iostream>
#include <stdio.h>

#include "Structures.h"
#include "Functions.h"
#include "DeletionFunctions.h"


using namespace std;

#ifdef MSVS
#  define not !
#  define and &&
#  define or ||
#endif

#define debug(x) std::cerr << __FILE__ << " (" << __LINE__ << ") " << #x << " == " << (x) << std::endl;

int main(int argc,char** argv)
{
	node *pRoot = nullptr;
	int order;
	if ((argc == 3) && (std::string(argv[argc - 2]) == std::string{"-i"}))
	{
        std::cout << __LINE__ << std::endl;
        debug(argc);
		debug(argc - 2);
		string s = argv[argc - 1];
        debug(s);
		openfile(s, order, pRoot);
	}
	else 
	{
		string s = "Commands.txt";  // tego by nie robil
		openfile(s, order, pRoot);
		cout << "Incorrect form of input data!";
		return 0;
	}
		
	/*string s = "Commands.txt";
	openfile(s, order, pRoot);*/
	//operation of reading the file and its commands
	/*int order = 3;
	for (int i : { 6, 4, 3, 8, 11, 15, 5, 20, 25, 30, 7, 9, 35, 67, 12, 98, 65, 34, 78, 99, 4, 3, 2, 36, 100, 80, 37, 10, 50, 34, 103, 96, 24, 79, 60, 74, 49, 95, 14, 16, 28, 60, 8, 8, 8, 7, 7, 7, 7, 68, 12, 14, 16 })
		add(pRoot, i, order);
	graph(pRoot, 0);
	removeValue(pRoot, 4);
	graph(pRoot,0);
	print(pRoot);*/
	//deleting memory leaks
	deleteTree(pRoot);
	delete pRoot;
	
    return 0;
}

